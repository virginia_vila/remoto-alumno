@extends('layouts.logout')

@section('tittle')
Tableboard
@endsection

@section('estilos')
     <link rel="stylesheet" href="..\..\assets/css/style.css">
     <link rel="stylesheet" href="..\..\css\styles.css">   
     <link rel="stylesheet" href="..\..\css\dashboard.css">  
     <link rel="stylesheet" href="..\..\css\test_scr_tableboard.css">  
@endsection


@section('content')

          <div id="app">

                 <vir-tableboard-component
                    :user="{{ json_encode($user) }}"
                    :opponent="{{ json_encode($opponent) }}"
                    :game="{{ json_encode($game) }}">
                </vir-tableboard-component> 

          </div>



    <script src="..\..\js\vTableboard.js"></script>
@endsection
