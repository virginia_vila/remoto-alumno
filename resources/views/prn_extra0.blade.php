@extends('layouts.app')

@section('tittle')
Gutenberg
@endsection

@section('estilos')
     <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="css\cartas.css">
     <link rel="stylesheet" href="css\styles.css">   
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
     <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"> -->
@endsection

@section('content')

<div class="container">
    <!-- TITULO --> 
    <div class="row">     
            <div class="col-sm-12 contenido_marron"> 
            <h1>Gutemberg</h1>
            </div>
    </div>

    <!-- CONTENIDO 1--> 
    <div class="row">  
            <div class="col-sm-12 contenido_blanco"> 

            <img src="img\01 Gutenberg.jpg" alt="Gutenberg"  class="float-left retrato img-thumbnail" width="25%">
            <p>Gutenberg <b>nació en Maguncia, Alemania alrededor de 1400 en la casa paterna llamada zum Gutenberg</b>. Su apellido verdadero es Gensfleisch (en dialecto alemán renano este apellido tiene semejanza, si es que no significa, «carne de ganso», por lo que el inventor de la imprenta en Occidente prefirió usar el apellido por el cual es conocido). </p>
            
            <p>Hijo del comerciante Federico Gensfleisch, <b>que adoptaría posteriormente hacia 1410 el apellido zum Gutenberg</b>, y de Else Wyrich, hija de un tendero. Conocedor del arte de la fundición del oro, se destacó como herrero para el obispado de su ciudad. La familia se trasladó a <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-link-45deg" viewBox="0 0 16 16">
               
                <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"/>
                <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"/>
                </svg> <a href="https://es.wikipedia.org/wiki/Eltville_am_Rhein"><b> Eltville am Rhein</a></b> , ahora en el Estado de Hesse, donde Else había heredado una finca. Debió haber estudiado en la Universidad de Erfurt, en donde está registrado en 1419 el nombre de Johannes de Alta Villa (Eltvilla). Ese año murió su padre. </p>
            
            <p>Nada más se conoce de Gutenberg hasta que en 1434 residió como platero en Estrasburgo, donde cinco años después se vio envuelto en un proceso, que demuestra de forma indudable, que Gutenberg había formado una sociedad con Hanz Riffe para desarrollar ciertos procedimientos secretos. En 1438 entraron como <b>asociados Andrés Heilman y Andreas Dritzehen </b> (sus herederos fueron los reclamantes) y en el expediente judicial se mencionan los términos de prensa, formas e impresión. </p>
            
            <p>De regreso a Maguncia, <b>formó una nueva sociedad con Johann Fust</b>, quien le da un préstamo con el que, en 1449, publicó el <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-link-45deg" viewBox="0 0 16 16">
               
               <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"/>
               <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"/>
               </svg> <a href="https://es.wikipedia.org/wiki/Misal_de_Constanza"><b> Misal de Constanza</a></b> , primer libro tipográfico del mundo occidental. Recientes publicaciones, en cambio, aseguran que este misal no pudo imprimirse antes de 1473 debido a la confección de su papel, por lo que no debió ser obra de Gutenberg. En 1452, Gutenberg da comienzo a la edición de la Biblia de 42 líneas (también conocida como Biblia de Gutenberg).   </p>

            <p>En 1455, Gutenberg carecía de solvencia económica para devolver el préstamo que le había concedido Fust, por lo que se disolvió la unión y Gutenberg se vio en la penuria (incluso tuvo que difundir el secreto de montar imprentas para poder subsistir). Johannes Gutenberg murió arruinado en Maguncia, Alemania el 3 de febrero de 1468. A pesar de la oscuridad de sus últimos años de vida, <b>siempre será reconocido como el inventor de la imprenta moderna</b>.</p>
            
            <p><i>Fuente Wikipedia</i></p>

            </div>
    </div>

    <!-- TITULO 2 --> 
    <div class="row">  
            <div class="col-sm-12 contenido_gris"> 
            <h2>¿Qué es lo que inventó? </h2>
            </div>
    </div>

    <!-- CONTENIDO 2--> 
    <div class="row">  
            <div class="col-sm-12 contenido_blanco"> 


            <p>El nombre de Gutenberg lo asociamos a la invención de la imprenta, pero mucho antes que él ya se imprimía sobre pergamino o papel. Un breve recorrido histórico nos indica que: </p>

            <ul>
            <li>2000 años antes de Gutenberg, en Roma se imprimían carteles con signos grabados en arcilla. </li>    
            <li>1400 años antes de Gutenberg, en China se imprimían carteles utilizando signos grabados en madera. Así en el año 686 se imprimió una publicación que se llamó “El sutra del diamante”, con signos grabados en una única madera. </li> 
            </ul>
                  <div class="text-center">
                  <a href="img\02 Sutra del diamante .png" data-lightbox="photos"><img class="img-responsive center-block" src="img\02 Sutra del diamante .png" alt="Gutenberg" style="width: 450px; height: 264px; object-fit: cover;"><p>El sutra del diamante</p></a>
                  <a href="img\03 Tipos chinos en madera.jpg" data-lightbox="photos"><img class="img-responsive center-block" src="img\03 Tipos chinos en madera.jpg" alt="Gutenberg" style="width: 450px; height: 264px; object-fit: cover;"><p>Tipos chinos en madera</p></a>
                  </div>

                  <div class="text-center">
                  <a href="img\04 Fundidor de tipos de Gutenberg.png" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\04 Fundidor de tipos de Gutenberg.png" alt="Gutenberg" style="width: 450px; height: 264px; object-fit: cover;"><p>Fundidor de tipos</p></a>
                  <a href="img\05 tipos moviles metal gutenberg.jpeg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\05 tipos moviles metal gutenberg.jpeg" alt="Gutenberg" style="width: 450px; height: 264px; object-fit: cover;"><p>Móviles metal gutenberg</p></a>
                  </div>
    </div>
    </div>
 <!-- CONTENIDO 3--> 
     <div class="row">  
            <div class="col-sm-12 contenido_blanco">            

                <p>Pero los moldes de madera tenían un problema: pronto se desgastaban y se echaban a perder. La gran idea de Gutenberg fue moldear piezas de metal para cada letra, creando de tipos de letras de metal individuales para la imprenta. Después sólo quedaba componer en una cajita, letra a letra, el texto que se quería imprimir, cajita que se manchaba con unos tampones entintados.Finalmente, sobre las letras metálicas entintadas se colocaba el papel y se presionaba con un aparato así. </p>
                <div class="text-center">
                <a href="img\06 tipos moviles metal gutenberg.jpg" data-lightbox="photos"><img class="center img-fluid img-thumbnail" src="img\06 tipos moviles metal gutenberg.jpg" alt="Gutenberg" style="width: 400px; height: 264px; object-fit: cover;"></a>
                <a href="img\07 Prensa_de_Gutenberg Replica.png" data-lightbox="photos"><img class="center img-fluid img-thumbnail" src="img\07 Prensa_de_Gutenberg Replica.png" alt="Gutenberg" style="width: 400px; height: 264px; object-fit: cover;"></a>
                </div> 
            </div>
    </div>
    
 <!-- CONTENIDO 4--> 
    <div class="row">  
            <div class="col-sm-12 contenido_gris"> 
                <p>En resumen, Gutenberg confeccionó un abecedario con letras y signos de plomo. Su idea fue eficaz porque la perfeccionó con:</p>
                <ul>
                <li>Letras móviles</li>
                <li>Molde de metal</li>
                <li>Fundidor de tipos o aparato de fundición manual</li>
                <li>Aleación especial de metales para fabricar los móviles (plomo, antimonioy bismuto)</li>
                <li>Prensa de madera anclada al suelo y al techo</li>
                <li>Tinta de imprimir en un determinado papel</li>            
                </ul>
            </div>
    </div>
    

     <!-- SIGUIENTE PÁGINA --> 
    <div class="row">     
            <div class="col-sm-12 contenido_marron navegador" style="margin-bottom: 20px;"> 
            <p><a href="./trabajos-imprenta" class="next">&#8250; Siguiente</a></p>
            </div>
    </div>
       
</div>

@endsection