@extends('layouts.app')

@section('tittle')
Imprenta
@endsection

@section('estilos')
     <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="css\home.css">
     <link rel="stylesheet" href="css\styles.css">   
@endsection


@section('content')
<!-- CABECERA -->
<div>
   <img src="img\cabecera-imprenta.jpg" class="d-block w-100">
</div>


<!-- BLOQUE 1 -->

<div class='container-fluid'>
  <div class='row' style="background-color: #e0ccb8;">

       <div class='col-sm-6'>
            <img class="img-section" src='img/scrabble-foto.jpg' alt='scrabble'>
        </div>

        <div class='col-sm-6'>
        <div class='text bordes'>
        <p id="cita"><b>¿Preparado para comenzar?</b></p>
        <h2>¡<b>Scrabble</b> te está esperando! </h2>
        <p>El poder de las palabras es tuyo</p>
        <a href="./scrabble" class="btn btn-lg " type="button">Juega</a>
        </div>
        </div>

  </div>
</div>

<!-- BLOQUE 2 -->

<div class='container-fluid'>
  <div class='row' style="background-color: white;">

        <div class='col-sm-6'>
        <h1><b>550 aniversario</b></h1>
        <h2>del fallecimiento de Johannes Gutenberg</h2>
        <br>  
        <p id="cita">“Como una nueva estrella la prensa dispersará la oscuridad de la ignorancia, y hará que entre los hombres brille una luz hasta ahora desconocida” </p>
        </div>

        <div class='col-sm-6'>
        <img class="img-section" src='img\book-turning-pages.jpg' alt='libros'>
        </div>
  </div>
</div>


<!-- BLOQUE 3 -->
<div class="container-fluid" id="imprenta">
   <h2>Información sobre la Imprenta</h2>      
  </div>


   
<!-- CARTAS DE INFORMACION -->
<div id="imprentainfo">

      <div class="card-deck">
        <div class="card card-border" style="max-width: 22rem;">
            <a href="./gutenberg"><img class="card-img-top" src="img\card01.jpg" alt="Card image cap"></a>
            <div class="card-body">
            <h5 class="card-title">Gutenberg</h5>
          </div>
        </div>

        <div class="card card-border" style="max-width: 22rem;">
            <a href="./trabajos-imprenta"><img class="card-img-top" src="img\card02.jpg" alt="Card image cap"></a>
            <div class="card-body">
            <h5 class="card-title">Trabajos imprenta</h5>
          </div>
        </div>


        <div class="card card-border" style="max-width: 22rem;">
            <a href="./difusion-imprenta"><img class="card-img-top" src="img\card03.jpg" alt="Card image cap"></a>
            <div class="card-body">
            <h5 class="card-title">Difusión imprenta</h5>
          </div>
        </div>

        <div class="card card-border" style="max-width: 22rem;">
            <a href="./primeros-libros"><img class="card-img-top" src="img\card04.jpg" alt="Card image cap"></a>
            <div class="card-body">
            <h5 class="card-title">Primeros libros</h5>
          </div>
        </div>

          <div class="card card-border" style="max-width: 22rem;">
            <a href="./imprenta-valenciana"><img class="card-img-top" src="img\card05.jpg" alt="Card image cap"></a>
            <div class="card-body">
            <h5 class="card-title">Imprenta valenciana</h5>
          </div>
          </div>
      </div>
      </div>
</div>



<!-- SCRABBLE -->
<div class="container-fluid" id="scrabble">
   <h2>Recuerda que puedes jugar al <b>Scrabble</b> aquí</h2>      
   <a href="./scrabble" class="btn btn-lg " type="button">Empezar</a>
</div>


  @endsection