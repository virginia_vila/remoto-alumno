@extends('layouts.app')

@section('tittle')
Trabajos imprenta
@endsection

@section('estilos')
     <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="css\cartas.css">
     <link rel="stylesheet" href="css\styles.css">   
@endsection

@section('content')
<div class="container">
    <!-- TITULO --> 
    <div class="row">     
            <div class="col-sm-12 contenido_marron"> 
            <h1>Los trabajos en una Imprenta del siglo XV</h1>
            </div>
    </div>


<!-- INTRODUCCION --> 
    <div class="row">  
            <div class="col-sm-12 contenido_blanco"> 
            <h5>Por eso, en una imprenta se necesitaban fundamentalmente <b>3 personas</b>:</h5>           
            </div>
    </div>
<!-- CONTENIDO CON PILLS-->
<div class="row">  
       <div class="col-sm-12 contenido_gris"> 

       <ul class="nav nav-tabs" style="color: white;">
            <li class="active pillstab"><a data-toggle="tab" href="#menu1"><button type="button" class="btn btn-light">El componedor</button></a></li>
            <li><a class="pillstab" data-toggle="tab" href="#menu2"><button type="button" class="btn btn-light">El entintador</button></a></li>
            <li><a class="pillstab" data-toggle="tab" href="#menu3"><button type="button" class="btn btn-light">El tirador</button></a></li>
        </ul>
        <div class="tab-content">
            <div id="menu1" class="tab-pane active">
                           <div class="text-center">
                                <div class="pills-trabajos">
                                <h3 class="titulo-clarito">1. El componedor</h3>
                                <p> Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en una cajita, una a una, todas las piezas de metal con letras y espacios que forman una línea. Debe hacerlo en orden inverso. Y cajita a cajita, confecciona toda una página. </p>
                                <img class="image" src="img\08b Trabajadores imprenta 1.png" alt="imprenta1" style="width: 35%">
                                </div>
                            </div>
            </div>
            <div id="menu2" class="tab-pane fade">
                             <div class="text-center">
                                <div class="pills-trabajos">
                                <h3 class="titulo-clarito">2. El entintador</h3>
                                <p>Encargado de entintar la superficie de letras que ha elaborado el componedor. Par ello utiliza dos tampones semiesféricos impregnados de tinta, uno en cada mano. </p>
                                <img class="image" src="img\09b Trabajadores imprenta 2.jpg" alt="imprenta2" style="width: 50%">
                                </div>
                            </div>
            </div>
            <div id="menu3" class="tab-pane fade">
            <div class="text-center">
                                <div class="pills-trabajos">
                                <h3 class="titulo-clarito">3. El tirador</h3>
                                <p> Coloca papel sobre la superficie de letras entintadas y acciona la palanca que hace bajar la prensa sobre los tipos metálicos que colocó el componedor, de manera que quedan marcadas en el papel. </p>
                                <img class="image" src="img\10b Trabajadores imprenta 3.jpg" alt="imprenta3" style="width:30%">
                                </div>
                            </div>
            </div>
        </div>

       </div>
</div>     


<!-- VIDEO --> 
<div class="row">  
            <div class="col-sm-12 contenido_blanco" style="text-align: center;">  
            <a href="https://www.youtube.com/watch?v=M2SanMKYdKk">
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" fill="currentColor" class="bi bi-youtube" viewBox="0 0 16 16"> <path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/> </svg>
            </a>     
            <h4>Video del proceso de <b>impresión con prensa</b></h4>
            </div>

    </div>



<!-- SIGUIENTE PÁGINA -->
     <div class="row">     
            <div class="col-sm-12 contenido_marron navegador" style="margin-bottom: 20px;"> 
            <p><a href="./difusion-imprenta" class="next">&#8250; Siguiente</a></p>
            </div>
    </div>
       

</div>
@endsection



