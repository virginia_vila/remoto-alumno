
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('tittle')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    @yield('estilos')
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
</head>
<body>


<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="font-size: 18px">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="./">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="./" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Imprenta
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="./gutenberg">Gutenberg</a>
          <a class="dropdown-item" href="./trabajos-imprenta">Trabajos imprenta</a>
          <a class="dropdown-item" href="./difusion-imprenta">Difusión</a>
          <a class="dropdown-item" href="./primeros-libros">Primeros libros</a>
          <a class="dropdown-item" href="./imprenta-valenciana">Imprenta valenciana</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./scrabble">Scrabble</a>
      </li>
    </ul>
  </div>
  <form class="form-inline">
        <a class="btn btn-dark" href="/scrabble/login">Login</a>
  </form>  
</nav>

@yield('content')


<!-- Footer -->
<footer class="text-center text-lg-start text-muted">

            <!-- Section: Links  -->
            <section class="">
              <div class="container text-center text-md-start mt-5">
                <!-- Grid row -->
                  <!-- Grid column -->
                  <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                    <!-- Content -->
                    <h6 class="text-uppercase fw-bold mb-4">


                <!-- Grid row -->
              </div>
            </section>


                  <!-- Copyright -->
                  <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                  <i class="fas fa-gem me-3"></i>CEEDCV</h6>
                    <p>Tu centro de estudios a distancia, público y gratuito de Castellón, València y Alicante.</p>
                    © 2021 Copyright:        
                       <p id="licencias" data-toggle="modal" data-target="#exampleModalLong">Licencias</p>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">

                            
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Haga click para ver las licencias</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                    <p>   Personas scrabble <a href='https://www.freepik.es/vectores/personas'>Vector de Personas creado por pch.vector - www.freepik.es</a></p> 
                                    <p>   Icono libro <a href='https://www.freepik.es/vectores/fondo'>Vector de Fondo creado por mamewmy - www.freepik.es</a><p>
                                    <p>   Podium <a href='https://www.freepik.es/vectores/personaje'>Vector de Personaje creado por jcomp - www.freepik.es</a></p> 
                                    <p>   Foto cabecera <a href='https://www.freepik.es/fotos/libro'>Foto de Libro creado por freepik - www.freepik.es</a></p> 
                                    <p>   Foto libros <a href='https://www.freepik.com/photos/background'>Background photo created by jcomp - www.freepik.com</a></p> 
                                    <p>   Foto start <a href='https://www.freepik.com/photos/background'>Background photo created by freepik - www.freepik.com</a></p> 

                                    <p>   https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg licencia bajo Dominio público</p> 
                                    <p>   https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png licencia bajo Dominio público</p> 
                                    <p>   https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg licencia bajo CC BY-SA 3.0 </p> 
                                    <p>   http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf</p> 
                                    <p>   https://pixabay.com/es/photos/escribiendo-conjunto-de-plomo-letras-1889146/ licencia bajo Pixabay License (No necesario reconocimiento)</p> 
                                    <p>   https://pixabay.com/es/photos/caso-gancho-de-%c3%a1ngulo-tipografia-705674/ licencia bajo Pixabay License (No necesario reconocimiento)</p> 
                                    <p>   https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._R%C3%A9plica..png licencia bajo Dominio público</p> 
                                    <p>   https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg licencia bajo Dominio público</p> 
                                    <p>   https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg licencia bajo Dominio público</p> 
                                    <p>   https://de.wikipedia.org/wiki/Datei:Press1520.png licencia bajo Dominio público</p> 
                                    <p>   https://pixabay.com/es/vectors/europa-los-paises-mapa-3d-rojo-151589/  licencia bajo Pixabay License (No necesario reconocimiento)</p> 
                                    <p>   https://bvpb.mcu.es/es/consulta/registro.cmd?id=405381 licencia Minesterio Cultura y Deporte</p> 
                                    <p>   https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG licencia bajo Creative Commons CC0 License</p> 
                                    <p>   https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg licencia bajo Dominio público</p> 
                                    <p>  https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg Licencia Creative Commons Atribución Compartir Igual 3.0</p> 
                                  
                                    <p>  Portal Valldigna. València. Foto de José M. Marín. Con autorización de publicación. </p> 
                                    <p>  Placa imprenta Palmart. Foto de José M. Marín. Con autorización de publicación Calle San Vicente numero 3 de València. </p> 
                                    <p>  Placa imprenta Patricio Mey. Foto de José M. Marín. Con autorización de publicación </p> 

                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div>
                          </div>
                        </div>

                  </div>
          </footer>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>

