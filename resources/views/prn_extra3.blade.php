@extends('layouts.app')

@section('tittle')
Primeros libros
@endsection

@section('estilos')
     <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="css\cartas.css">
     <link rel="stylesheet" href="css\styles.css">   
@endsection


@section('content')


<div class="container">

       <!-- TITULO --> 
    <div class="row">     
            <div class="col-sm-12 contenido_marron"> 
            <h1>Los primeros libros en España</h1>
            </div>
    </div>



 <!-- INTRO -->
    <div class="row">  
       <div class="col-sm-12 contenido_gris"> 
             <p> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle-fill" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
            </svg>  El primer libro impreso en España fue el <b>El sinodal de Aguilafuente en 1472</b> en Segovia.</p>  

        </div>
    </div>


<!-- CONTENIDO -->
    <div class="row">  
       <div class="col-sm-12 contenido_blanco">       
        <p> Por su parte, los tres primeros impresos en València con el procedimiento de Gutenberg fueron:</p>     
        
        <div id="accordion">
                         <div class="card">
                            <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Obres o trobes en laors de la Verge Maria - 1474
                                </button>
                            </h5>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            <img id="myimage" src="img\12 les trobes.jpg" width="100%">
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Comprehensorium - 1475
                                </button>
                            </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            <img id="myimage" src="img\13 el comprensorium.jpg" width="100%">
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Biblia valenciana - 1478
                                </button>
                            </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                            <img src="img\14 valencian bible.jpg" alt="Comprensorium" width="100%">
                            </div>
                            </div>
                        </div>
                        </div>
        </div>
        </div>

 <!-- SIGUIENTE PÁGINA -->
                <div class="row">     
            <div class="col-sm-12 contenido_marron navegador" style="margin-bottom: 20px;"> 
            <p><a href="./imprenta-valenciana" class="next">&#8250; Siguiente</a></p>
            </div>
    </div>
</div>

@endsection