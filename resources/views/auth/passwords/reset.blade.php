@extends('layouts.app')

@section('tittle')
Reiniciar contraseña
@endsection
           

@section('estilos')
     <link rel="stylesheet" href="..\..\..\assets/css/style.css">
     <link rel="stylesheet" href="..\..\..\css\styles.css">   
     <link rel="stylesheet" href="..\..\..\css\login-registro.css">  
@endsection


@section('content')
<div class='container'>
    <div class='row text-center'> 
    <div class='col-sm-12 recuperar'>

    <div class="form">
    <img class="img-section" src='..\..\..\img\reset2.png' alt='scrabble' width="60%">
        <form method="POST" class="caja" action="{{ route('password.request') }}">
            @csrf <!-- por razones educativas está desactivado -->

            <input type="hidden" name="token" value="{{ $token }}">

            <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" placeholder="Correo" required autofocus>
            <input id="password" type="password" name="password" placeholder="Contraseña" required>
    
            <input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirme contraseña" required>
            <br>      
                @if ($errors->isNotEmpty())
                        <div class="error">
                            <div>                           
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach                            
                            </div>
                        
                    @endif
                
            <button type="submit" class="btn btn-lg">Reiniciar contraseña</button> 
                  
        </form>
        </div>
        </div>
    </div>
 </div>  

 @endsection
