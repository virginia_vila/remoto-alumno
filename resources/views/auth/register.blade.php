@extends('layouts.app')

@section('tittle')
Registro
@endsection

@section('estilos')
     <link rel="stylesheet" href="..\assets/css/style.css">
     <link rel="stylesheet" href="..\css\styles.css">   
     <link rel="stylesheet" href="..\css\login-registro.css">  
@endsection

@section('content')


<style>


</style>

<div class='container'>

        <div class='row text-center'>
                <div class='col-sm-12 registro1'>
                <img class="img-section" src='..\img\registro.png' alt='scrabble' width="50%">
                </div>
        </div>       

        <div class='row text-center'>
                <div class='col-sm-12 registro2'>


                <div class="form">
                           <form class="caja" action="{{ route('register') }}" method="post">
                                <input id="name" type="text" name="name" value="{{ old('name') }}" placeholder="Usuario" required autofocus>
                                <input id="email" type="text" name="email" value="{{ old('email') }}" placeholder="Correo" required>
                                <input id="password" type="password" name="password" placeholder="Contraseña" required>
                                <input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirmación contraseña" required>
                                <input id="country" type="text" name="country" placeholder="Pais" required>  
                                  @if ($errors->isNotEmpty())
                                        <div class="error">
                                            <div>
                                               
                                                    @foreach ($errors->all() as $error)
                                                        <p>{{ $error }}</p>
                                                    @endforeach
                                               
                                            </div>
                                            <!-- <div>
                                                <h4>Error modo 2</h4>
                                                @if ($errors->has('email'))
                                                    <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                                                @endif
                                            </div> -->
                                        </div>
                                    @endif   
                                <button type="submit" class="btn btn-lg">Registrar </button> 
                            </form>
                </div>     

              

                
                </div>
       </div>         
       

</div>



@endsection