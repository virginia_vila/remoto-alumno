@extends('layouts.app')

@section('tittle')
Error 500
@endsection

@section('estilos')
     <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="css\styles.css">   
     <link rel="stylesheet" href="css\errores.css">  
@endsection

@section('content')


<div class="container">
      <div class="text-center">
         <img src="img\error500.png" class="mx-auto d-block" width="60%">

        <h1 class="error-tittle"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="tuerca bi bi-nut-fill" viewBox="0 0 111.87 111.34">
        <path class="cls-1" d="M57.17,99.79a2.12,2.12,0,0,1,1.55-1.57l14.15-3.41a2.09,2.09,0,0,1,2.1.69c2.49,3,8.94,5.65,12.37,6.62l13.43-12.76c-.8-3.48-3.14-10.06-6-12.7a2.08,2.08,0,0,1-.59-2.12l4.12-14a2.09,2.09,0,0,1,1.64-1.47c3.84-.66,9.38-4.92,11.93-7.41l-4.34-18c-3.41-1-10.28-2.31-14-1.15A2.09,2.09,0,0,1,91.4,32l-10-10.55a2.09,2.09,0,0,1-.45-2.16c1.35-3.66.43-10.57-.45-14L62.69,0c-2.61,2.43-7.13,7.74-8,11.54a2.06,2.06,0,0,1-1.55,1.57L39,16.53a2.07,2.07,0,0,1-2.09-.69c-2.5-3-8.95-5.66-12.38-6.63L11.11,22c.79,3.48,3.13,10.05,6,12.69a2.08,2.08,0,0,1,.59,2.13l-4.12,14a2.06,2.06,0,0,1-1.64,1.46C8.09,52.9,2.56,57.15,0,59.64l4.35,18c3.41,1,10.27,2.31,14,1.15a2.08,2.08,0,0,1,2.13.55l10,10.55A2.1,2.1,0,0,1,31,92.06c-1.34,3.66-.43,10.58.45,14l17.77,5.24C51.79,108.91,56.31,103.59,57.17,99.79Zm4.09-22.06A22.69,22.69,0,1,1,78,50.34,22.7,22.7,0,0,1,61.26,77.73Z"/>
        </svg>ERROR 500</h1>
        <h2 class="error-subtittle">Internal server error</h2>
        <p class="error-description">El servidor ha fallado y no puede suministrar la solicitud.</p>
        <a href="./" class="btn btn-lg " type="button">Volver a la home</a>
        </div>
</div>


@endsection