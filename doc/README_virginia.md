# Espacio de trabajo
- Sistema Operativo: Windows 10 Pro
- Editor de codigo: Visual Studio Code
- Framework: Bootstrap

# Herramientas adicionales
- Adobe Illustrator
- Freepik

# Licencias imágenes 

- Personas scrabble <a href='https://www.freepik.es/vectores/personas'>Vector de Personas creado por pch.vector - www.freepik.es</a>
- Icono libro <a href='https://www.freepik.es/vectores/fondo'>Vector de Fondo creado por mamewmy - www.freepik.es</a>
- Podium <a href='https://www.freepik.es/vectores/personaje'>Vector de Personaje creado por jcomp - www.freepik.es</a>
- Foto cabecera <a href='https://www.freepik.es/fotos/libro'>Foto de Libro creado por freepik - www.freepik.es</a>
- Foto libros <a href='https://www.freepik.com/photos/background'>Background photo created by jcomp - www.freepik.com</a>
- Foto start <a href='https://www.freepik.com/photos/background'>Background photo created by freepik - www.freepik.com</a>

- https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg licencia bajo Dominio público
- https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png licencia bajo Dominio público
- https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg licencia bajo CC BY-SA 3.0 
- http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf
- https://pixabay.com/es/photos/escribiendo-conjunto-de-plomo-letras-1889146/ licencia bajo Pixabay License (No necesario reconocimiento)
- https://pixabay.com/es/photos/caso-gancho-de-%c3%a1ngulo-tipografia-705674/ licencia bajo Pixabay License (No necesario reconocimiento)
- https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._R%C3%A9plica..png licencia bajo Dominio público
- https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg licencia bajo Dominio público
- https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg licencia bajo Dominio público
- https://de.wikipedia.org/wiki/Datei:Press1520.png licencia bajo Dominio público
- https://pixabay.com/es/vectors/europa-los-paises-mapa-3d-rojo-151589/  licencia bajo Pixabay License (No necesario reconocimiento)
- https://bvpb.mcu.es/es/consulta/registro.cmd?id=405381 licencia Minesterio Cultura y Deporte
- https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG licencia bajo Creative Commons CC0 License
- https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg licencia bajo Dominio público
- https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg Licencia Creative Commons Atribución Compartir Igual 3.0

- Portal Valldigna. València. Foto de José M. Marín. Con autorización de publicación. 
- Placa imprenta Palmart. Foto de José M. Marín. Con autorización de publicación Calle San Vicente numero 3 de València. 
- Placa imprenta Patricio Mey. Foto de José M. Marín. Con autorización de publicación 
