@extends('layouts.app')

@section('tittle')
Reinicio contraseña
@endsection
           

@section('estilos')
     <link rel="stylesheet" href="..\..\assets/css/style.css">
     <link rel="stylesheet" href="..\..\css\styles.css">   
     <link rel="stylesheet" href="..\..\css\login-registro.css">  
@endsection


@section('content')
<div class='container'>
    <div class='row text-center'> 
       <div class='col-sm-12 recuperar'>
                    <div class="form">
                    <div class="card-body">
                    <img class="img-section" src='..\..\img\reset.png' alt='scrabble' width="60%">
                        <form class="caja" method="POST" action="{{ route('password.email') }}">
                            @csrf <!-- por razones educativas está desactivado -->

                            <p>Introduzca su correo para recuperar su contraseña mediante un enlace</p>
                            <input id="email" type="email" name="email" placeholder="Correo" required>
                            <br>
                            @if ($errors->isNotEmpty())
                                <div class="error">
                                    <div>
                                            @foreach ($errors->all() as $error)
                                                <p>{{ $error }}</p>
                                            @endforeach              
                                    </div>

                                </div>
                            @endif
                            @if (session('status'))
                                <div class="informacion">
                                    {{ session('status') }}       
                                </div>
                            @endif
                            <button type="submit" class="btn btn-lg">Envía el enlace</button> 
                     </form>
                    </div>    
        </div>
        </div>
    </div>
    </div>

@endsection
