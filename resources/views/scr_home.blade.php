@extends('layouts.logout')

@section('tittle')
Dashboard
@endsection

@section('estilos')
     <link rel="stylesheet" href="..\assets/css/style.css">
     <link rel="stylesheet" href="..\css\styles.css">   
     <link rel="stylesheet" href="..\css\dashboard.css">  
     <link rel="stylesheet" href="..\css\test_scr_dashboard.css">  
@endsection


@section('content')

@php
    if (is_null($user->avatar)) $avatar = "";
    else $avatar = $user->avatar;
@endphp


          <div id="app">
               <vir-dashboard-component
                :user="{{ json_encode($user) }}"
                :avatar="{{ json_encode($avatar) }}"
                :variables="{{ json_encode($statistics) }}"
                :games="{{json_encode($games)}}"
               
                >
                </vir-dashboard-component> 
          </div>
            <!-- form oculto para realizar el logout via POST de manera síncrona. Podría haber utilizado la función post
                que está definida en test_helpers -->
            <!-- <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf                
            </form> -->



    <!-- <script src="..\js\test_scr_home.js"></script> -->
    <script src="..\js\vDashboard.js"></script>
@endsection
