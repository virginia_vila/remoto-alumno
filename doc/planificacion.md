# PFM-F1  
## PLANIFICACIÓN  
## VIRGINIA VILA COTILLAS  


## PERSONAS
### LUCIA CALPE
#### PERSONA 1
![lucia](./pic/lucia.jpg)  
**Edad:** 18 años  
**Población:** Alboraya  
**Ocupación:** Estudiante  
**Hobbies:** Redes sociales  

Lucía es una adolescente que acaba de entrar actualmente a 1º de la ESO. No le entusiasma ir a clase, pero le encanta pasar tiempo con sus amigos y en las redes sociales.  

-  **Conocimiento sobre el campo que trata la web:** Bajo. Por una tarea de la escuela, tiene que visitar esta página, aunque Lucía no muestra mucho interés por aprender ni saber sobre la imprenta.  
- **Objetivo en el site:** En clase les han enviado la tarea de que el fin de semana tendrán que leerse la información sobre la imprenta y probar el juego del scrabble, para que el lunes en clase pongan en común.  
- **Preocupaciones:** Su preocupación es que pueda entenderlo todo bien para cuando el lunes en clase le pregunten saber contestar adecuadamente.  
- **Conocimientos tecnológicos:** Lucía es una adolescente que le encanta usar las redes sociales y está todo el día con su móvil, por lo que se desenvuelve bien con las tecnologías y su intuición en las páginas web es buena.  
- **Escenario:** Es domingo por la tarde, Lucía se acaba de acordar que tiene la tarea de la imprenta y el scrabble para el lunes. Va a la sala de ordenadores en su casa y busca en su libre en enlace para acceder a la página web. Lee un poco la información por encima y juega una partida al Scrabble.  

### SVEN LARSEN
#### PERSONA 2
![sven](./pic/sven.jpg)  
**Edad:** 25 años  
**Población:** Copenhague  
**Ocupación:** Estudiante / Empleado  
**Hobbies:** Videojuegos  

Sven es un joven danés que ha estudiado el grado de Ingeneria Informática y se está especializando con un Master en la creación de videojuegos. Además, trabaja a jornada parcial en un restaurante de fast food donde conoció a su novia Patricia. 

- **Conocimiento sobre el campo que trata la web:** Bajo, a Sven le encantan las nuevas tecnologías y el tema imprenta/lectura no es algo que le apasione 
- **Objetivo en el site:** Sven no es muy apasionado de los juegos de mesa, pero al enterarse de que el Scrabble está de manera online le apetece probarlo.
- **Preocupaciones:** Respecto a la página, su preocupación es no saber jugar o que el juego no sea lo suficientemente entretenido. 
- **Conocimientos tecnológicos:** Sven es un gran apasionado de la tecnología y los videojuegos. Puede estar horas delante de su ordenador jugando o investigando sobre como mejorar su ordenador.
- **Escenario:** Es por la tarde entre semana, Sven y su novia Patricia están en casa pasando el rato tranquilamente. A Patricia le llega un Whatsapp de su hermano que está estudiando en el ceed comentándole que han puesto el Scrabble online ya que de pequeños les encantaba jugar juntos. Patricia piensa que es buena idea enseñarselo a Sven. Acceden a la página y juegan una partida. A Sven le ha entretenido y jugará más veces durante esa semana.   

### CARMEN SOLER
#### PERSONA 3
![carmen](./pic/carmen.jpg)  
**Edad:** 42 años  
**Población:** Valencia  
**Ocupación:** Empleada  
**Hobbies:** Televisión y leer  

Carmen trabaja en unas oficinas en el mundo relacionado con el Marketing. Le encanta el café, ver la televisión y leer, asiste habitualmente a ferias del libro.  

- **Conocimiento sobre el campo que trata la web:** Medio-alto, su afición a la lectura hace que Carmen tenga algunos conocimientos sobre como comenzó todo.  
- **Objetivo en el site:** Informarse un poco más sobre la historia y ver si van a haceralgún evento relacionado con la lectura.  
- **Preocupaciones:** Que no vayan a hacer un evento relacionado al que poder asistir presencialmente.  
- **Conocimientos tecnológicos:** Carmen podría considerarse un usuario medio, sedesenvuelve bien, utiliza normalmente su smartphone y su portatil, pero no es una gran apasionada de las nuevas tecnologías.  
- **Escenario:** Carmen se encuentra en su trabajo, es el descanso y una compañera le ha comentado sobre el aniversario de la imprenta este año. Buscando en Google ha encontrado la página web. Indiga unos 15 minutos por la página, le ha gustado mucho la información que ha recopilado sobre la imprenta, pero ella esperaba más, hubiera preferido que hubieran hecho algún evento al que poder asistir.


### ANTONIO PRAT
#### PERSONA 4
![antonio](./pic/antonio.jpg)  
**Edad:** 66 años  
**Población:** Picassent  
**Ocupación:** Jubilado  
**Hobbies:** Lectura  

Antonio ha dedicado toda su vida a trabajar en una antigua biblioteca, una de sus grandes aficiones ha sido leer, or lo que siempre ha sido su trabajo soñado. Es un gran fanático y conoce grandes detalles sobre este mundo.

- **Conocimiento sobre el campo que trata la web:** Alto, al haber trabajado toda la vida en una biblioteca, es un tema que controla totalmente, no solo por trabajo, si no por afición.  
- **Objetivo en el site:** Curiosear la información relacionada con la imprenta y encontrar nuevos recursos que le puedan ser de interés.  
- **Preocupaciones:** Que la información que pueda encontrar en esta página no cumpla con sus especativas.  
- **Conocimientos tecnológicos:** Bajo, Antonio no está acostumbrado a las nuevas tecnologías, pero se ha enterado por un anuncio de esta página web. Intentará acceder por su cuenta, si no les pedirá ayuda a sus hijos.   
- **Escenario:** Es un viernes por la tarde y la hija de Antonio Prats se ha encontrado el Scrabble buscando minijuegos online, ve que la página web es en conmemoración a la imprenta, y no duda en cometárselo a su padre. Le pasa en enlace a Antonio, el cual entusiasmado, se pasa toda la tarde navegando por ella. Además se ha suscrito para recibir todas las novedades.

## MAPA CONCEPTUAL SCRABBLE  
![mapaconceptual](./pic/mapaconceptual.jpg)

## MAPA DEL SITIO WEB  
![mapaweb](./pic/mapaweb.jpg)

## DIAGRAMA DE FLUJO DE UNA PARTIDA  
![diagrama](./pic/diagrama.jpg)


## MODELOS ALÁMBRICOS 
### HOME  
![home](./pic/home.jpg)
### INFORMACIÓN IMPRENTA  
![imprenta](./pic/imprenta.jpg)
### SCRABBLE  
![scrabble](./pic/scrabble.jpg)
### DASHBOARD  
![dashboard](./pic/dashboard.jpg)
### TABLERO  
![Tablero](./pic/tablero.jpg)