@extends('layouts.app')

@section('tittle')
Login
@endsection
           

@section('estilos')
     <link rel="stylesheet" href="..\assets/css/style.css">
     <link rel="stylesheet" href="..\css\styles.css">   
     <link rel="stylesheet" href="..\css\login-registro.css">  
@endsection

@section('content')


<div class="contener">
<div class="text-typing">
  <p>Te estábamos esperando&nbsp;</p>
</div>
</div>


<div class='container'>
        <div class='row login-content' >
                <div class='col-sm-6 login1 text-center'>
                <img class="img-section" src='..\img\login.png' alt='scrabble' width="80%">
                </div>

                <div class='col-sm-6 login2'> 
           
                         <div class="form">
                            <h2>Bienvenido</h2>
                            <form class="box" method="POST" action="{{ route('login') }}">
                                
                                @csrf <!-- por razones educativas está desactivado -->
                                <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="email" required autofocus>
                                <br>
                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="password" name="password" required>           
                               
                                @if ($errors->isNotEmpty())
                    <div class="error">
                        <div>
                                @foreach ($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            
                        </div>
                        <!-- <div>
                      
                            @if ($errors->has('email'))
                                <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                            @endif
                        </div> -->
                    </div>
                @endif 
                                <a href="{{ route('password.request') }}"><p class="olvidadopass">¿Has olvidado tu contraseña?</p></a>
                                <button class="btn btn-lg" type="submit">Login</button>   
                                </div>
                            </form>
                        </div>     

                                    

                </div>
        </div>
</div>

@endsection