@extends('layouts.app')

@section('tittle')
Imprenta valenciana
@endsection


@section('estilos')
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="css\cartas.css">
<link rel="stylesheet" href="css\styles.css">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
@endsection


@section('content')

<div class="container">

 <!-- TITULO --> 
           <div class="row">     
            <div class="col-sm-12 contenido_marron"> 
            <h1>Lugares emblemáticos de la imprenta valenciana</h1>
            </div>
    </div>

  <!-- INTRO -->
  <div class="row">  
       <div class="col-sm-12 contenido_gris"> 
             <p> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
            </svg> Haz clic en las imágenes para descubrir la ubicación.</p>  
        </div>
    </div>   

<!-- CONTENIDO -->

     <div class="row">  
            <div class="col-sm-12 contenido_blanco">      
                <div class="text-center">
                    <h3 class="valencia">El Molí de Rovella y El Monasterio de Santa María del Puig</h3>
                    <a href="img\mapa1.jpg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\15 Valencia.Mercado_Central.jpg" alt="valencia"  style="width: 400px; height: 264px; object-fit: cover;"></a>
                    <a href="img\mapa2.jpg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\16 El Puig Monasterio.jpg" alt="valencia" style="width: 400px; height: 264px; object-fit: cover;"></a>
                    <p>En la confluencia de las actuales calles Barón de Cárcer y <b><a href="https://goo.gl/maps/WLq98GznccGH6SUj7">Pie de la Cruz</a></b> estuvo ubicada la primera imprenta en València. El<b><a href="https://goo.gl/maps/f8mnye9d5xQ3HYgh6El">Mosterio del Santa María</a></b> alberga el Museo de la Imprenta y contiene una réplica exacta de la imprenta utilizada Gutenberg y que se conserva en Maguncia (Alemania).</p>
                </div> 
                <div> 
            </div>  
        </div>    
     </div>

<!-- CONTENIDO -->

<div class="row">  

     </div>

<!-- CONTENIDO -->

<div class="row">  
            <div class="col-sm-12 contenido_blanco">  
              <div class="text-center">    
                <p class="valencia">Imprenta de Palmart</p>      
                <a href="img\mapa3.jpg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\17 portal valldigna 1.jpg" alt="valencia" style="width: 400px; height: 264px; object-fit: cover;"></a>
                <a href="img\mapa3.jpg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\17 portal valldigna 2.jpg" alt="valencia" style="width: 400px; height: 264px; object-fit: cover;"></a>
                <p>Junto al <b><a href="https://goo.gl/maps/WLq98GznccGH6SUj7">Portal de la Valladigna</a></b> se situaron los talleres de imprenta de donde salieron: “Trobes en loors de la Verge María” “Comprehensorium” “Biblia valenciana” .</p>
                </div> 
            </div>     
     </div>

<!-- CONTENIDO -->

<div class="row">  
            <div class="col-sm-12 contenido_blanco"> 
             <div class="text-center">          
                <p class="valencia">Imprenta de Patricio Mey</p>   
                <a href="img\mapa4.jpg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\19 calle san vicente patricio mey 2.jpg" alt="valencia" style="width: 400px; height: 264px; object-fit: cover;"></a>
                <a href="img\mapa4.jpg" data-lightbox="photos"><img class="img-fluid img-thumbnail" src="img\19b calle san vicente Patricio May.png" alt="valencia" style="width: 400px; height: 264px; object-fit: cover;"></a>   
                <p>En el <b><a href="https://goo.gl/maps/8mxGYPQ23ASfaddU7">número 3 de la calle San Vicente</a></b> se imprimió la segunda edición de “Don Quijote de la Mancha”.</p>
            </div>     
            </div> 
     </div>



 <!-- SIGUIENTE PÁGINA -->
     <div class="row">     
            <div class="col-sm-12 contenido_marron navegador" style="margin-bottom: 20px;"> 
            <p><a href="./scrabble" class="next">&#8250; ¡Ahora a jugar!</a></p>
            </div>
    </div>
</div>


@endsection
